package com.redturtle.ca7s.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.redturtle.ca7s.R

class SplaceScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splacescreen)

        Handler().postDelayed({ startActivity(Intent(applicationContext,LoginActivity::class.java))
            finish() },4000)
    }

}
