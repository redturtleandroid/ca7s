package com.redturtle.ca7s.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.widget.CardView
import android.view.View
import android.view.View.*
import android.widget.Button
import com.redturtle.ca7s.R



class SignupActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var sigwitEmail : Button
    private lateinit var sigwitPhone : Button
    private lateinit var siplayEmail : TextInputLayout
    private lateinit var siplayPhone : TextInputLayout
    private lateinit var Submit : CardView
    //btn_ScardSignup



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        sigwitEmail = findViewById(R.id.btn_SEmail)
        sigwitPhone =  findViewById(R.id.btn_SPhone)
        siplayEmail = findViewById(R.id.txInLay_Email)
        siplayPhone = findViewById(R.id.txInLay_Phone)
        Submit = findViewById(R.id.btn_ScardSignup)

        sigwitEmail.setOnClickListener(this)
        sigwitPhone.setOnClickListener(this)
        Submit.setOnClickListener(this)
        sigwitPhone.alpha = 0.2F

    }
    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.btn_SEmail -> {
                if (v.id == R.id.btn_SEmail ) {
                    siplayEmail.visibility = VISIBLE
                    siplayPhone.visibility = GONE
                    sigwitPhone.alpha = 0.2F
                    sigwitEmail.alpha = 1.0F
                }
            }
            R.id.btn_SPhone -> {
                if (v.id == R.id.btn_SPhone ) {
                    siplayPhone.visibility = VISIBLE
                    siplayEmail.visibility = GONE
                    sigwitEmail.alpha = 0.2F
                    sigwitPhone.alpha = 1.0F
                }
                //sigwitEmail.alpha = 0.2F
                // Toast.makeText(this, "twoButton", Toast.LENGTH_SHORT).show()
            }
            R.id.btn_ScardSignup -> {
                startActivity(Intent(applicationContext,ProfileActivity::class.java))
            }
        }

    }


}



