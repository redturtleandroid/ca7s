package com.redturtle.ca7s.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.redturtle.ca7s.R
import com.redturtle.ca7s.adapter.MyMusic_List_Adpt
import com.redturtle.ca7s.adapter.mainplay_adpt

class MainPlayActivity : AppCompatActivity(),mainplay_adpt.clicklist {


    private lateinit var ardiogroup: RadioGroup
    private lateinit var rbfavourite : RadioButton
    private lateinit var rbmusic : RadioButton
    private lateinit var rbbroadcast : RadioButton
    private lateinit var rbsearch : RadioButton
    private lateinit var layout: LinearLayoutManager
    private lateinit var recycler: RecyclerView
    private lateinit var text1 :TextView
    private var arr = arrayOf("Aorem ipsum","Alorem ipsum","Ason ipsum","Atrorem  ipsum","Bsdefh ipsum","Baalenb ipsum","Csrm ipsum","Cftorem ipsum","Gonorem ipsum","Ykrrem ipsum")
    private var arrs = arrayOf("semba","Antihances","kizomba ipsum","despers rock","chiph ipsum","thrills ipsum","arget ipsum"," cget ipsum","nnon ipsum","hyikem ipsum")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_play)
        recycler=findViewById(R.id.mainmusicrv)
        text1=findViewById(R.id.texttitle)
        ardiogroup=findViewById(R.id.radiogrp)
        layout= LinearLayoutManager(this)
        recycler.layoutManager=layout
        recycler.setHasFixedSize(true)
        recycler.itemAnimator=DefaultItemAnimator()
        var title :String = intent.getStringExtra("arrayname")
        text1.text=title
        val thisadpt = mainplay_adpt(this,arr,arrs,this)
        recycler.adapter=thisadpt

        //radiogroup
        ardiogroup.setOnCheckedChangeListener { group, checkedId ->
            rbfavourite=group.findViewById(R.id.rbfav)
            rbmusic=group.findViewById(R.id.rbmusic)
            rbbroadcast=group.findViewById(R.id.rbbroad)
            rbsearch=group.findViewById(R.id.rbsearch)

            when(ardiogroup.checkedRadioButtonId) {
                R.id.rbfav->{
                    startActivity(Intent(applicationContext,Favourites::class.java))
                    rbfavourite.isChecked=false
                }
                R.id.rbmusic -> {
                    startActivity(Intent(applicationContext,MyMusicActivty::class.java))
                    rbmusic.isChecked = false

                }
                R.id.rbbroad -> {
                    startActivity(Intent(applicationContext,BroadcastActivity::class.java))
                    rbbroadcast.isChecked=false

                }
                R.id.rbsearch -> {
                    startActivity(Intent(applicationContext,SearchActivty::class.java))
                    rbsearch.isChecked = false

                }
            }
        }

    }
    override fun onclicklist(pos: Int) {
        val intent = Intent(applicationContext,MusicPlayerActivity::class.java)
        intent.putExtra("title",arr[pos])
        intent.putExtra("subtitle",arrs[pos])
        startActivity(intent)

    }
}
