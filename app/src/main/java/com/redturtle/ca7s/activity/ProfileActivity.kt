package com.redturtle.ca7s.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import com.redturtle.ca7s.R

class ProfileActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var EditProfile : RelativeLayout
    private lateinit var Follower : RelativeLayout
    private lateinit var Following : RelativeLayout
    private lateinit var Favorites : RelativeLayout
    private lateinit var My_Music : RelativeLayout
    private lateinit var Add_Music : RelativeLayout
//img_prook_click rl_favrit
    private lateinit var okClick : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        EditProfile =  findViewById(R.id.rl_editpro)
        Follower = findViewById(R.id.rl_follower_block)
        Following = findViewById(R.id.rl_folowing_block)
        okClick = findViewById(R.id.img_prook_click)
        Favorites =  findViewById(R.id.rl_favrit)
        My_Music = findViewById(R.id.rl_music)
        Add_Music = findViewById(R.id.rl_AddMusic)

        EditProfile.setOnClickListener(this)
        Follower.setOnClickListener(this)
        Following.setOnClickListener(this)
        okClick.setOnClickListener(this)
        Favorites.setOnClickListener(this)
        My_Music.setOnClickListener(this)
        Add_Music.setOnClickListener(this)


    }

    override fun onClick(v: View?) {

        when(v!!.id) {

            R.id.rl_editpro ->{
                startActivity(Intent(applicationContext,EditProfileActivity::class.java))
            }
            R.id.rl_follower_block ->{
                startActivity(Intent(applicationContext,FollowersActivity::class.java))
            }
            R.id.rl_folowing_block ->{
                startActivity(Intent(applicationContext,FollowingActivity::class.java))
            }
            R.id.img_prook_click -> {
                //startActivity(Intent(applicationContext,Favourites::class.java))
            }
            R.id.rl_favrit ->{
                startActivity(Intent(applicationContext,Favourites::class.java))
            }
            R.id.rl_music ->{
                startActivity(Intent(applicationContext,MyMusicActivty::class.java))
            }
            R.id.rl_AddMusic ->{
                startActivity(Intent(applicationContext,AddMusicActivity::class.java))
            }

        }
    }
}
