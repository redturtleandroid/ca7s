package com.redturtle.ca7s.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.redturtle.ca7s.R

class MusicPlayerActivity : AppCompatActivity() {

    private lateinit var textmusic:TextView
    private lateinit var textdescription:TextView
    private lateinit var Cancle:ImageView
    //img_MusicPlay_cancle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_player)
        textmusic=findViewById(R.id.mpmusictv)
        textdescription=findViewById(R.id.mpsubtitletv)
        Cancle = findViewById(R.id.img_MusicPlay_cancle)


        textmusic.text=intent.getStringExtra("title")
        textdescription.text=intent.getStringExtra("subtitle")
        Cancle.setOnClickListener {
            val intent = Intent(this@MusicPlayerActivity, Favourites::class.java)
            startActivity(intent)
        }

    }
}
