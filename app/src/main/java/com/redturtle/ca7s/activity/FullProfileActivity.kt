package com.redturtle.ca7s.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.redturtle.ca7s.R

class FullProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_profile)
    }
}
