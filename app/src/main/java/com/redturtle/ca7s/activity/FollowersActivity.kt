package com.redturtle.ca7s.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.redturtle.ca7s.ClickInterface.PosInterface
import com.redturtle.ca7s.R
import com.redturtle.ca7s.adapter.FollowersAdapter

class FollowersActivity : AppCompatActivity() , PosInterface {

    internal var follo = arrayOf("Follow+","Followed","Followed","Follow+","Followed","Follo+","Followed","Followed")
    internal lateinit var folloRecyclerView: RecyclerView
    private lateinit var mLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_followers)

        folloRecyclerView = findViewById(R.id.recyler_follower)

        folloRecyclerView.setHasFixedSize(true)
        mLayoutManager = LinearLayoutManager(applicationContext)
        folloRecyclerView.layoutManager = mLayoutManager

        val followAdapter = FollowersAdapter(follo,this , this)
        folloRecyclerView.adapter = followAdapter
    }

    override fun onPositionSelect(position: Int) {
        val intent = Intent(this@FollowersActivity, FullProfileActivity::class.java)
        startActivity(intent)
    }

}
