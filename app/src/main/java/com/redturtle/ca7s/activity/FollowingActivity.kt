package com.redturtle.ca7s.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.redturtle.ca7s.ClickInterface.PosInterface
import com.redturtle.ca7s.R
import com.redturtle.ca7s.adapter.FollowersAdapter

class FollowingActivity : AppCompatActivity() , PosInterface {


    internal var follo = arrayOf("Followed","UnFollow","Followed","Followed","Followed","UnFollow","UnFollow","Followed")
    internal lateinit var folowingRecyclerView: RecyclerView
    private lateinit var mLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_following)

        folowingRecyclerView = findViewById(R.id.recyler_following)

        folowingRecyclerView.setHasFixedSize(true)
        mLayoutManager = LinearLayoutManager(applicationContext)
        folowingRecyclerView.layoutManager = mLayoutManager

        val followAdapter = FollowersAdapter(follo,this , this)
        folowingRecyclerView.adapter = followAdapter
    }

    override fun onPositionSelect(position: Int) {
        val intent = Intent(this@FollowingActivity, FullProfileActivity::class.java)
        startActivity(intent)
    }
}
