package com.redturtle.ca7s.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.CardView
import com.redturtle.ca7s.R

class LoginActivity : AppCompatActivity() {

    private lateinit var Login : CardView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        Login = findViewById(R.id.btn_LcardLogin)

        Login.setOnClickListener {
            startActivity(Intent(applicationContext,SignupActivity::class.java))
        }

    }
}
