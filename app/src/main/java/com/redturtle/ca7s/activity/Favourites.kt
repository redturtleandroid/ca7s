package com.redturtle.ca7s.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import com.redturtle.ca7s.R
import android.support.v4.view.ViewPager
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

import java.nio.file.Files.size
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.widget.Toolbar
import com.redturtle.ca7s.fragment.AlbumFragment
import com.redturtle.ca7s.fragment.PlaylistFragment
import com.redturtle.ca7s.fragment.SongsFragment


class Favourites : AppCompatActivity() {

    private lateinit var ardiogroup: RadioGroup
    private lateinit var rbfavourite : RadioButton
    private lateinit var rbmusic : RadioButton
    private lateinit var rbbroadcast : RadioButton
    private lateinit var rbsearch : RadioButton
    private lateinit var toolbar: Toolbar
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourites)
        ardiogroup=findViewById(R.id.radiogrp)

       /* toolbar =  findViewById(R.id.toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)*/

        viewPager =  findViewById(R.id.viewpager)
        setupViewPager(viewPager)

        tabLayout = findViewById(R.id.tabs)
        tabLayout.setupWithViewPager(viewPager)


        ardiogroup.check(R.id.rbfav)
        ardiogroup.setOnCheckedChangeListener(object:RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {

                rbfavourite=group.findViewById(R.id.rbfav)
                rbmusic=group.findViewById(R.id.rbmusic)
                rbbroadcast=group.findViewById(R.id.rbbroad)
                rbsearch=group.findViewById(R.id.rbsearch)



                when(ardiogroup.checkedRadioButtonId) {
                    R.id.rbbroad -> {
                        startActivity(Intent(applicationContext,BroadcastActivity::class.java))
                        rbbroadcast.isChecked=false
                        rbfavourite.isChecked = true
                    }
                    R.id.rbmusic -> {
                        startActivity(Intent(applicationContext,MyMusicActivty::class.java))
                        rbmusic.isChecked = false
                        rbfavourite.isChecked=true
                    }
                    R.id.rbsearch -> {
                        startActivity(Intent(applicationContext,SearchActivty::class.java))
                        rbsearch.isChecked = false
                        rbfavourite.isChecked = true
                    }
                }
            }
        })
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(AlbumFragment(), "Album")
        adapter.addFragment(SongsFragment(), "Songs")
        adapter.addFragment(PlaylistFragment(), "Playlist")
        viewPager.adapter = adapter
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList.get(position)
        }
    }

}
