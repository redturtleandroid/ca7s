package com.redturtle.ca7s.ClickInterface

/**
 * Created by RedTurtle-05 on 01-03-2018.
 */
interface PosInterface {
    fun onPositionSelect(position: Int)
}