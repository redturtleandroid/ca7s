package com.redturtle.ca7s.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.redturtle.ca7s.R


/**
 * A simple [Fragment] subclass.
 */
class AddLyricsFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_lyrics, container, false)
    }

}// Required empty public constructor
