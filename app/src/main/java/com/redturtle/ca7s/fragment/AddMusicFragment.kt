package com.redturtle.ca7s.fragment


import android.graphics.Canvas
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.redturtle.ca7s.R
import android.graphics.RectF
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth
import android.widget.ProgressBar
import android.widget.RelativeLayout


@Suppress("UNREACHABLE_CODE")
/**
 * A simple [Fragment] subclass.
 */
class AddMusicFragment : Fragment() {

    //private lateinit var EditProfile : RelativeLayout
   // private lateinit var progressBar : ProgressBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_music, container, false)

       /* progressBar = view!!.findViewById(R.id.circle_progress_bar)
        progressBar.setProgress(65)*/

    }

    /*protected fun onDraw(canvas: Canvas) {
        val centerXOnView = getWidth() / 2
        val centerYOnView = getHeight() / 2

        val viewXCenterOnScreen = getLeft() + centerXOnView
        val viewYCenterOnScreen = getTop() + centerYOnView

        val threeDpPad = resources.getDimension(R.dimen.three_dp)
        val rad = resources.getDimension(R.dimen.seventy_dp)

        val leftOffset = (viewXCenterOnScreen - (rad + threeDpPad * 4)).toInt()
        val topOffset = (viewYCenterOnScreen - (rad + threeDpPad * 3)).toInt()
        val rightOffset = (viewXCenterOnScreen + (rad + threeDpPad * 4)).toInt()
        val bottomOffset = (viewYCenterOnScreen + (rad + threeDpPad)).toInt()

        val oval = RectF(leftOffset.toFloat(), topOffset.toFloat(), rightOffset.toFloat(), bottomOffset.toFloat())

        var textLength = getText().length
        if (textLength % 2 != 0) {
            textLength = textLength + 1
        }
        this.myArc.addArc(oval, -90 - textLength * 2, 90 + textLength + 10)

        canvas.drawTextOnPath(getText() as String, this.myArc, 0, 10, this.mPaintText)
        invalidate()
    }*/

}// Required empty public constructor
