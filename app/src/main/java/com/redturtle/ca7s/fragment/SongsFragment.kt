package com.redturtle.ca7s.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import com.redturtle.ca7s.R
import com.redturtle.ca7s.adapter.song_frg_adpt


/**
 * A simple [Fragment] subclass.
 */
class SongsFragment : Fragment() {

    private lateinit var linear : LinearLayoutManager
    private lateinit var recycler: RecyclerView
    private var arraytitle = arrayOf<String>("Aorem ipsum","Alorem ipsum","Ason ipsum","Atrorem  ipsum","Bsdefh ipsum","Baalenb ipsum","Csrm ipsum","Cftorem ipsum","Gonorem ipsum","Ykrrem ipsum")
    private var arraysdescr = arrayOf<String>("lorem ipsum","Alom ipsum","dummy ipsum","despers  ipsum","chiph ipsum","thrills ipsum","arget ipsum"," cget ipsum","nnon ipsum","hyikem ipsum")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_songs,container,false)
        recycler=view.findViewById(R.id.frgsongrv)
        linear= LinearLayoutManager(activity)
        recycler.layoutManager=linear
        recycler.setHasFixedSize(true)
        recycler.itemAnimator=DefaultItemAnimator()

        val songadpt=song_frg_adpt(arraytitle,arraysdescr,view.context)
        recycler.adapter=songadpt

        return view
    }

}// Required empty public constructor
