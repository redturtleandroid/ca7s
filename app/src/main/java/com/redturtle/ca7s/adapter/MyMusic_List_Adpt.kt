package com.redturtle.ca7s.adapter

import android.content.Context
import android.support.v7.view.menu.MenuView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.redturtle.ca7s.R

/**
 * Created by Red Turtle-04 on 26-02-2018.
 */
class MyMusic_List_Adpt : RecyclerView.Adapter<MyMusic_List_Adpt.Myviewholder> {

    private var arra = arrayOf<String>()
    private var sarra = arrayOf<String>()
    private var context:Context

    constructor(arra: Array<String>, sarra: Array<String>, context: Context) : super() {
        this.arra = arra
        this.sarra = sarra
        this.context = context
    }

    override fun onBindViewHolder(holder: Myviewholder?, position: Int) {
        holder!!.titlet.text=arra[position]
        holder!!.stitlet.text=sarra[position]

    }
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Myviewholder{
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.row_mymusic_adpt,parent,false)
        return Myviewholder(view)
    }
    override fun getItemCount(): Int {
       return arra.size
    }

    inner class Myviewholder(itemvieww : View):RecyclerView.ViewHolder(itemvieww)
    {
        var titlet:TextView = itemvieww.findViewById(R.id.titletv)
        var stitlet:TextView = itemvieww.findViewById(R.id.subtitlead)

    }


}