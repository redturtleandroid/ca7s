package com.redturtle.ca7s.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.redturtle.ca7s.ClickInterface.PosInterface
import com.redturtle.ca7s.R


class FollowersAdapter :RecyclerView.Adapter<FollowersAdapter.MyFolloHolder>{

    var addfolowering  = arrayOf<String>()
    var context: Context
    private var posinterface : PosInterface

    constructor(addfolowering: Array<String>, context: Context, posinterface: PosInterface) : super() {
        this.addfolowering = addfolowering
        this.context = context
        this.posinterface = posinterface
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyFolloHolder{
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.row_follo_wer_wing,parent,false)
        return MyFolloHolder(view)
    }

    override fun getItemCount(): Int {
        return addfolowering.size
    }

    override fun onBindViewHolder(holder: MyFolloHolder?, position: Int) {
        holder!!.textalbum.text = addfolowering[position]
        holder.customeView.setOnClickListener {
            posinterface.onPositionSelect(position)
        }

    }

    inner class MyFolloHolder(itemview : View):RecyclerView.ViewHolder(itemview)
    {
        var customeView : View = itemView
        var textalbum: TextView = itemview.findViewById(R.id.txt_add_folower)
    }

}