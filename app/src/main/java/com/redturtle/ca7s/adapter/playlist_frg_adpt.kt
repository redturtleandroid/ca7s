package com.redturtle.ca7s.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.redturtle.ca7s.R

/**
 * Created by Red Turtle-04 on 28-02-2018.
 */
class playlist_frg_adpt : RecyclerView.Adapter<playlist_frg_adpt.MyViewHolder> {

    private var context: Context
    private  var album  = arrayOf<String>()
    private var click : onclickfragment

    constructor(context: Context, album: Array<String>, click: onclickfragment) : super() {
        this.context = context
        this.album = album
        this.click = click
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int):MyViewHolder {

        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.row_playlist_adpt,parent,false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        holder!!.textalbum.text = album[position]
    }

    override fun getItemCount(): Int {
       return album.size
    }

    inner class MyViewHolder(itemview : View) : RecyclerView.ViewHolder(itemview)
    {
        var textalbum: TextView = itemview.findViewById(R.id.playalbumtitleiv)

        init {
            itemview.setOnClickListener {
                click.clickitem(position)
            }
        }


    }

  interface onclickfragment {
        fun clickitem(ipo :Int)
    }
}